# README #

Summary:

This project is a part of the Cannonball project. It is a Mac client to collect sensor data available on Mac. 

Currently supported sensors are:

1. IDLE: the time in seconds of the period of inactivity of the Mac keyboard and mouse pad
2. CPU: the cpu usage in percentage of the Mac
3. Memory: the physical memory usage in percentage of the Mac


How to run:

Build the project in Eclipse with all required libraries. 
Run "MainActivity" in src/main.

Project Structure:

-client: contains Listener and Intent class to listen to the socket.io server for requests
-database: contains Database class using JDBC driver and SQLite; local database
-main: contais Main class
-ui: UI is the main user interface; Registration is the registration page
-utils: contains HTTPHandler for data sending, and Constants of server addresses

Author:

Kangwei Chen