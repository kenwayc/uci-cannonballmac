package common;

/**
 * Created by joyceshin on 2015. 4. 30..
 */
public enum ServiceType {

    PUT_USER,
    PUT_PLATFORM,
    PUT_SENSOR,
    PUT_OBSERVATION, PUT_LOG
}
