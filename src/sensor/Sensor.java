package sensor;

import java.sql.SQLException;

public abstract class Sensor {

	int SID;
	String name;
	int platformId;
	
	SensorType type;
	String IP;			/* We need to make sure whether we need this for each sensor */
	
	float resolution;	/* only for Camera */
	
	public Sensor() {};
	
	public Sensor(int id, String name, String ip)
	{
		SID = id;
		this.name = name;
		IP = ip;
	}
	
	public Sensor( String name, int PlatformId, SensorType type) 
	{
		platformId = PlatformId;
		this.type = type;
		this.name = name;
		this.IP = null;
	}
	
	public Sensor( String name, int PlatformId, SensorType type, String ip) 
	{
		platformId = PlatformId;
		this.type = type;
		this.name = name;
		IP = ip;
	}
	
	/*
	public Result createSensor(SensorType type){
		return Result.SUCCESS;
	}
	public Result destroySensor(SensorType type){
		return Result.SUCCESS;
	}
	*/
	
	/* getters */
	public String getIp()
	{
		return IP;
	}
	
	public String getName(SensorType type) {
		return name;
	}
	
	public SensorType getType() {
		return type;
		
	}
	
	/* setters */
	public void setResolution(float resolution) {
		this.resolution = resolution;
	}
	
	public void setSensorType(SensorType type)
	{
		this.type = type;
	}
	
	public void setPlatformId(int id)
	{
		platformId = id;
	}
	
	public String toString()
	{
		return this.name + " - Platform id: "  + this.platformId +" - Sensor Type: " + this.type + " - " + this.IP;
	}
	
}
