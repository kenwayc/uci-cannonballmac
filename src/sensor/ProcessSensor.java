package sensor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ProcessSensor {

	private String processName;
	private String sessionName;
	private String memoryUsage;
	private String timestamp;
	private String upTime;
	
	public ProcessSensor() {
		this.processName = "";
		this.sessionName = "";
		this.memoryUsage = "";
		this.timestamp = ""; 
	}
/*	
	public ProcessSensor(String processName, String sessionName, String memoryUsage, String upTime) {
		this.processName = processName;
		this.sessionName = sessionName;
		this.memoryUsage = memoryUsage;
		this.upTime = upTime;
	}
*/	
	public ProcessSensor(String processName, String sessionName, String memoryUsage) {
		this.processName = processName;
		this.sessionName = sessionName;
		this.memoryUsage = memoryUsage;
	}
	
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	
	public String getProcessName() {
		return processName;
	}
	
	public void setSessionName(String sessionName) {
		this.sessionName = sessionName;
	}
	
	public String getSessionName() {
		return sessionName;
	}
	
	public void setMemoryUsage(String memoryUsage) {
		this.memoryUsage = memoryUsage;
	}
	
	public String getMemoryUsage() {
		return memoryUsage;
	}
	
	public String getTimeStamp(){
		return this.timestamp;
	}
	
	public void setTimeStamp(String timestamp){
		this.timestamp = new String(timestamp);
	}
	
	//get current time
	public String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd-HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
	}
	
	
}
