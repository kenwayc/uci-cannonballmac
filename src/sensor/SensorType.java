package sensor;

/**
 * 
 * @author Kenway
 * This enum. type variable defines sensor data types.
 *
 */
public enum SensorType {
	IDLETIME,		//	1
	CPU,				//  2
	MEMORY,			//	3
	PROCESS			// 4
}
