package sensor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class Point {
	private int id;
	private String state;
	private Integer idle_time;
	private Double cpu;
	private Double memory;
	private String timestamp;
	public interface State {
		String unkown = "UNKOWN";
		String online = "ONLINE";
		String idle = "IDLE";
		String away = "AWAY";
	}
	private ArrayList<ProcessSensor> processList;
	
	//constructor
	public Point() {
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String unkown) {
		this.state = unkown;
	}
	
	public int getIdleTime() {
		return this.idle_time;
	}
	
	public void setIdleTime(int idle_time) {
		this.idle_time = idle_time;
	}
	
	public double getCPU() {
		return this.cpu;
	}
	
	public void setCPU(Double cpu) {
		this.cpu = cpu;
	}
	
	public double getMemory() {
		return this.memory;
	}
	
	public void setMemory(Double memory) {
		this.memory = memory;
	}
	
	public void addProcess(ProcessSensor process) {
		this.processList.add(process);
	}
		
	public ArrayList<ProcessSensor> getProcessList() {
		return this.processList;
	}
	
	public void setProcessList(ArrayList<ProcessSensor> processList) {
		this.processList = processList;
	}
	
	public String getTimeStamp(){
		return this.timestamp;
	}
	
	public void setTimeStamp(String timestamp){
		this.timestamp = new String(timestamp);
	}
	
	//get current time
	public String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd-HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
	}

	//toString method
	public String toString() {
		return "IdleStatus [id = " + id + ", state = " + state + ", timestamp = " + timestamp + ", number of process =" + processList.size() + "]";
	}
}
