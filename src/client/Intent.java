package client;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import ui.UI;

public class Intent {
	private String type;
	private Timer timer1; //start collecting sensor data
	private Timer timer2; //end collecting sensor data
	private StartSensor sensorStart;
	private Date startTime;
	private Date endTime;
	private EndSensor sensorEnd;
	private DateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd-HH:mm:ss");
	private UI ui;
	
	class StartSensor extends TimerTask {
		public void run() {
			switch (type) { // turn on tracking of the sensor type
				case "idle":
					ui.getToggleButton().doClick(5);
					ui.setToggle(true);
					ui.getToggleButton().setText("Turn off tracking");
					System.out.println("Start Collecting IDLE");
					break;
				case "cpu":
					ui.getToggleButton().doClick(5);
					ui.setToggle(true);
					ui.getToggleButton().setText("Turn off tracking");
					System.out.println("Start Collecting CPU");
					break;
				case "memory":
					ui.getToggleButton().doClick(5);
					ui.setToggle(true);
					ui.getToggleButton().setText("Turn off tracking");
					System.out.println("Start Collecting MEMORY");
					break;
				case "process":
					ui.getToggleButton().doClick(5);
					ui.setToggle(true);
					ui.getToggleButton().setText("Turn off tracking");
					System.out.println("Start Collecting PROCESS");
					break;
			}
				
		}
	}
	
	class EndSensor extends TimerTask {
		public void run() {
			 // turn off tracking of the sensor type
			switch (type) { // send sensor data to server
			case "idle":
				ui.getToggleButton().doClick(5);
				ui.setToggle(false);
				ui.getToggleButton().setText("Turn on tracking");
				ui.getSendIDLEButton().doClick(10);
				System.out.println("End Collecting IDLE. Send");
				break;
			case "cpu":
				ui.getToggleButton().doClick(5);
				ui.setToggle(false);
				ui.getToggleButton().setText("Turn on tracking");
				ui.getSendCPUButton().doClick(10);
				System.out.println("End Collecting CPU. Send");
				break;
			case "memory":
				ui.getToggleButton().doClick(5);
				ui.setToggle(false);
				ui.getToggleButton().setText("Turn on tracking");
				ui.getSendMemoryButton().doClick(10);
				System.out.println("End Collecting Memory. Send");
				break;
			case "process":
				ui.getToggleButton().doClick(5);
				ui.setToggle(false);
				ui.getToggleButton().setText("Turn on tracking");
				ui.getSendProcessButton().doClick(10);
				System.out.println("End Collecting Process. Send");
			}
				
		}
	}
	
	public Intent(String sensorType, String start, String end, UI ui) {
		this.ui = ui;
		type = sensorType;
		try {
			startTime = dateFormat.parse(start);
			endTime = dateFormat.parse(end);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// create the intent
	public void create() {
		timer1 = new Timer();
		sensorStart = new StartSensor();
		timer1.schedule(sensorStart, startTime);
		timer2 = new Timer();
		sensorEnd = new EndSensor();
		timer2.schedule(sensorEnd, endTime);
	}
	

}
