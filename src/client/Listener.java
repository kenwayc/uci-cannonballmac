package client;

import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import main.MacIdleTime;
import main.Win32IdleTime;
import message.Message;

import org.json.JSONObject;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import database.Database;
import sensor.DesktopSensor;
import sensor.Point;
import ui.UI;
import utils.Constants;

public class Listener {
	private static UI ui;
	static Point entry;
	static String previous_state;
	static String current_state;
	static Double cpu, memory;
	static DecimalFormat df = new DecimalFormat("#.##");

	public Listener(MacIdleTime macidle, Database db, UI ui)
			throws URISyntaxException {
		this.ui = ui;
		run();
	}
	
	public Listener(Win32IdleTime winidle, Database db, UI ui)
			throws URISyntaxException {
		this.ui = ui;
		run();
	}

	// validate platformID
	static boolean validatePlatformID(String platformID) {
		return platformID.equalsIgnoreCase(DesktopSensor.platformId);
	}

	private static Emitter.Listener onNewMessage = new Emitter.Listener() {

		@Override
		public void call(Object... os) {
			// TODO Auto-generated method stub
			System.out.println("ON message Got");
			JSONObject data = (JSONObject) os[0];
			String platformID; // platform ID
			String type, startTime, endTime;
			long requestID;

			platformID = data.getString("platformID");
			type = data.getString("sensorType");
			startTime = data.getString("startTime");
			endTime = data.getString("endTime");
			requestID = data.getLong("requestID");
			System.out.println(platformID);

			if (validatePlatformID(platformID)) { // test
				DesktopSensor.requestID = requestID;
				//
				System.out.println("type " + type + " startTime " + startTime
						+ " requestID " + requestID);
				Intent intentStart;
				// intentStart = new Intent(msgArray[2], msgArray[3],
				// msgArray[4], ui);
				intentStart = new Intent(type, startTime, endTime, ui);
				intentStart.create();
			}
		}
	};

	public void run() throws URISyntaxException {
		IO.Options options = new IO.Options();
		options.forceNew = true;
		final Socket socket = IO.socket(Constants.SERVER_ADDRESS, options);
		socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
			@Override
			public void call(Object... args) {
				System.out.println("connect");
				socket.emit("chat message", "Java client");
				// socket.close();
			}
		}).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

			@Override
			public void call(Object... os) {
				System.out.println("Disconnect");
			}
		}).on("new message", onNewMessage);

		socket.connect();
		// socket.emit(event, args);

	}

	public static void main(String[] args) throws URISyntaxException,
			InterruptedException {
		IO.Options options = new IO.Options();
		options.forceNew = true;
		final Socket socket = IO.socket(Constants.SERVER_ADDRESS, options);
		socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
			@Override
			public void call(Object... args) {
				System.out.println("connect");
				socket.emit("chat message", "Java client");
				// socket.close();
			}
		}).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

			@Override
			public void call(Object... os) {
				System.out.println("Disconnect");
			}
		}).on("new message", onNewMessage);

		socket.connect();
		// Thread.sleep(1000);
		// socket.disconnect();
	}
}
