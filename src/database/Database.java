package database;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sensor.DesktopSensor;
import sensor.Point;
import sensor.ProcessSensor;

public class Database {

	private Connection connection = null;
	private final String dbName; // database file name
	private Statement statement;
	private String tableName , processTableName;
	//private DateFormat dateFormat = new SimpleDateFormat(
			//"EEE, d MMM yyyy HH:mm:ss");

	private static final String KEY_ACCURACY = "accuracy";
	private static final String KEY_PROVIDER = "provider";
	private static final String KEY_ID = "id";
	private static final String KEY_STATE = "state";
	private static final String KEY_IDLETIME = "idle_time";
	private static final String KEY_CPU = "cpu_usage";
	private static final String KEY_MEMORY = "memory_usage";
	private static final String KEY_CREATED_AT = "created_at";
	/* processes */
	private static final String KEY_PROCESS_IMAGE_NAME = "image_name";
	private static final String KEY_PROCESS_SESSION_NAME = "session_name";
	private static final String KEY_PROCESS_MEMORY_USAGE = "memory_usage";
//	private static final String KEY_PROCESS_UP_TIME = "up_time";
	
	int result = 0;

	// constructor
	public Database(String databaseName) {		
		if(System.getProperty("os.name").contains("Windows")) {
			String workingdirectory = System.getProperty("user.dir");
//			System.out.println(workingdirectory);
			
			dbName = workingdirectory + "\\" + databaseName;
		} else {
			dbName = databaseName;
		}
		
//		System.out.println(dbName);
		
		tableName = "IdleStatus";
		processTableName = "ProcessStatus";
	}

	// create a database
	public void createDB() throws ClassNotFoundException {
		Class.forName("org.sqlite.JDBC");
		try {
						
			String dbN = "jdbc:sqlite:" + dbName;
			connection = DriverManager.getConnection(dbN);
			statement = connection.createStatement();
			statement.setQueryTimeout(30);  // set timeout to 30 sec.
			
			result = statement.executeUpdate("drop table if exists " + tableName);
						
			result = statement.executeUpdate("create table " + tableName + "(" + KEY_ID
					+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_STATE
					+ " String, " + KEY_IDLETIME + " Integer, " + KEY_CPU
					+ " Double, " + KEY_MEMORY + " Double, " + KEY_CREATED_AT
					+ " DATETIME DEFAULT CURRENT_TIMESTAMP" + ")");
			
			result = statement.executeUpdate("drop table if exists " + processTableName);
			
			result = statement.executeUpdate("create table " + processTableName + "(" 
					+ KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " 
					+ KEY_PROCESS_IMAGE_NAME + " String, " 
					+ KEY_PROCESS_SESSION_NAME + " String, " 
					+ KEY_PROCESS_MEMORY_USAGE + " String, " 
					+ KEY_CREATED_AT + " DATETIME DEFAULT CURRENT_TIMESTAMP" + ")");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	// add entry to the database
	public void addEntry(Point entry) throws ClassNotFoundException {
		Class.forName("org.sqlite.JDBC");
		try {
			String dbN = "jdbc:sqlite:" + dbName;
			
			connection = DriverManager.getConnection(dbN);
			statement = connection.createStatement();
			
			entry.setTimeStamp(entry.getDateTime());
			result = statement.executeUpdate("insert into " + tableName + " (" + KEY_STATE + ", " + KEY_IDLETIME + ", " + KEY_CPU + ", "
					+ KEY_MEMORY + ", " + KEY_CREATED_AT + ")" + " values('"
					+ entry.getState() + "', '" + entry.getIdleTime() + "', '"
					+ entry.getCPU() + "', '" + entry.getMemory() + "', '"
					+ entry.getTimeStamp() + "')"); // in question
			
//			System.out.println("inserted entry=" + result);
			
			  
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// connection close failed.
				System.err.println(e);
			}
		}
	}

	// delete entry from the database
	public void deleteEntry(String key) throws ClassNotFoundException {
		Class.forName("org.sqlite.JDBC");
		try {
			connection = DriverManager.getConnection("jdbc:sqlite:" + dbName);
			statement = connection.createStatement();
			statement.executeUpdate("delete from " + tableName + " where id= "
					+ key); // in question
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// connection close failed.
				System.err.println(e);
			}
		}
	}
		
	// add entry to the database
	public void addProcessEntry(ProcessSensor entry) throws ClassNotFoundException {
		Class.forName("org.sqlite.JDBC");
		try {
			String dbN = "jdbc:sqlite:" + dbName;
			
			connection = DriverManager.getConnection(dbN);
			statement = connection.createStatement();
			
			entry.setTimeStamp(entry.getDateTime());
			result = statement.executeUpdate("insert into " + processTableName + " (" + KEY_PROCESS_IMAGE_NAME + ", " 
					+ KEY_PROCESS_SESSION_NAME + ", " + KEY_PROCESS_MEMORY_USAGE + ", "
					+ KEY_CREATED_AT + ")" + " values('"
					+ entry.getProcessName() + "', '" + entry.getSessionName() + "', '"
					+ entry.getMemoryUsage() + "', '"
					+ entry.getTimeStamp() + "')"); 
			
//			System.out.println("inserted entry result =" + result);
					  
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// connection close failed.
				System.err.println(e);
			}
		}
	}

	// delete entry from the database
	public void deleteProcessEntry(String key) throws ClassNotFoundException {
		Class.forName("org.sqlite.JDBC");
		try {
			connection = DriverManager.getConnection("jdbc:sqlite:" + dbName);
			statement = connection.createStatement();
			statement.executeUpdate("delete from " + processTableName + " where id= " + key); 
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// connection close failed.
				System.err.println(e);
			}
		}
	}

	// drop the table
	public void dropTable() throws ClassNotFoundException {
		Class.forName("org.sqlite.JDBC");
		try {
			connection = DriverManager.getConnection("jdbc:sqlite:" + dbName);
			statement = connection.createStatement();
			statement.executeUpdate("drop table " + tableName); //clear the table
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// connection close failed.
				System.err.println(e);
			}
		}
	}
	
	// print out database
	public List<Point> getAllEntries() throws ClassNotFoundException {
		List<Point> entries = new LinkedList<Point>();
		Class.forName("org.sqlite.JDBC");
		try {
			connection = DriverManager.getConnection("jdbc:sqlite:" + dbName);
			statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select * from " + tableName);
			Point entry = null;
			while (rs.next()) {
				// read the result set
				entry = new Point();
				entry.setId(rs.getInt(KEY_ID));
				entry.setState(rs.getString(KEY_STATE));
				entry.setIdleTime(rs.getInt(KEY_IDLETIME));
				entry.setCPU(rs.getDouble(KEY_CPU));
				entry.setMemory(rs.getDouble(KEY_MEMORY));
				entry.setTimeStamp(rs.getString(KEY_CREATED_AT));
				entries.add(entry);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// connection close failed.
				System.err.println(e);
			}
		}
		return entries;

	}


	public JSONObject exportToJSON() throws JSONException,
			FileNotFoundException, SQLException, ClassNotFoundException {

		Class.forName("org.sqlite.JDBC");

		connection = DriverManager.getConnection("jdbc:sqlite:" + dbName);
		statement = connection.createStatement();
		ResultSet rs = statement.executeQuery("select * from " + tableName);
		// SQLiteDatabase db = this.getWritableDatabase();

		// String query = "select * from location";
		// Cursor c = db.rawQuery(query, null);
		// c.moveToFirst();

		JSONObject Root = new JSONObject();
		JSONArray LocationArray = new JSONArray();
		File f = new File("pc.txt"); // TBD
		FileOutputStream fos = new FileOutputStream(f, true);
		PrintStream ps = new PrintStream(fos);

//		int i = 0;
		while (!rs.isAfterLast()) { // TBD

			/*
			 * "OID": 1111, "sensor": 1111, "Timestamp":
			 * "2012-04-21T18:25:43-05:00", "type": "Raw GPS", "data":{
			 * "longitude": 41.255, "latitude": 25.855, "altitude": 285, }
			 */
			JSONObject location = new JSONObject();
			try {
				location.put("OID", rs.getInt(KEY_ID));
				location.put("sensor", DesktopSensor.platformId); // device id
				location.put("Timestamp", rs.getString(KEY_CREATED_AT));
				location.put("type", "Raw Idle Time"); // sensor type

				JSONObject data = new JSONObject();
				data.put("state", rs.getString(KEY_STATE));
				data.put(KEY_IDLETIME, rs.getInt(KEY_IDLETIME));
				data.put(KEY_CPU, rs.getDouble(KEY_CPU));
				data.put(KEY_MEMORY, rs.getDouble(KEY_MEMORY));
				// data.put("Accuracy",
				// c.getString(c.getColumnIndex(KEY_ACCURACY)));
				// data.put("Provider",
				// c.getString(c.getColumnIndex(KEY_PROVIDER)));

				location.put("data", data);

				rs.next();

				LocationArray.put(location);

				// if( i == 1 )
				// break;

				// location_tot.put( String.valueOf(i), location );
//				i++;
				// return location;

			} catch (JSONException e) {

				e.printStackTrace();
				// return false;
			} finally {
				try {
					if (connection != null)
						connection.close();
				} catch (SQLException e) {
					// connection close failed.
					System.err.println(e);
				}
			}

		}

		// Root.put("LOCATIONS", location_tot);
		Root.put("observations", LocationArray);
		ps.append(Root.toString());

		return Root;
		// return true;
		// return Root.toString();
	}

	// IDLE sensor export to json
	public JSONObject exportToJSON_IDLE() throws JSONException,
			FileNotFoundException, SQLException, ClassNotFoundException {

		Class.forName("org.sqlite.JDBC");
		
		String dbN = "jdbc:sqlite:" + dbName;
		connection = DriverManager.getConnection("jdbc:sqlite:" + dbName);
			
		String query = "select * from " + tableName + ";";
				
//		System.out.println("select result=" + rs.getRow());
		
		JSONObject Root = new JSONObject();
//		JSONArray LocationArray = new JSONArray();
		File f = new File("Idle.txt"); // TBD
		FileOutputStream fos = new FileOutputStream(f, true);
		PrintStream ps = new PrintStream(fos);

		int i = 0;
		JSONObject idle = new JSONObject();
					
		try {
			
			statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(query);
						
			while (rs.next()) { // TBD	
				
				idle.put("type", "pc");

				JSONObject data = new JSONObject();
				data.put("OID", rs.getInt(KEY_ID));
				data.put("sensor", ""); 
				data.put("Timestamp", rs.getString(KEY_CREATED_AT));
				data.put("type", "IDLE"); 
				data.put("platformID", DesktopSensor.platformId); // platformID
				data.put("requestID", Long.toString(DesktopSensor.requestID)); // requestID
				data.put(KEY_IDLETIME, rs.getInt(KEY_IDLETIME));
				
				idle.put("data", data);
/*
				JSONObject data = new JSONObject();
				data.put(KEY_IDLETIME, rs.getInt(KEY_IDLETIME));
//				data.put(KEY_CPU, rs.getDouble(KEY_CPU));
//				data.put(KEY_MEMORY, rs.getDouble(KEY_MEMORY));
				location.put("data", data);
*/
//				rs.next();
//				LocationArray.put(location);
				i++;
				// return location;
			}
		} catch (JSONException e) {
			e.printStackTrace();
			// return false;
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// connection close failed.
				System.err.println(e);
			}
		}
			
//		Root.put("pc_observation", idle);
//		ps.append(Root.toString());
		
//		System.out.println("exportToJSON_IDLE=" + Root.toString());
		return idle;
	}

	// CPU sensor
	public JSONObject exportToJSON_CPU() throws JSONException,
			FileNotFoundException, SQLException, ClassNotFoundException {

		Class.forName("org.sqlite.JDBC");
		
		String dbN = "jdbc:sqlite:" + dbName;
		connection = DriverManager.getConnection(dbN);
			
		String query = "select * from " + tableName + ";";
//		System.out.println(query);
		
//		System.out.println("select result=" + rs.getRow());
		
		JSONObject Root = new JSONObject();
//		JSONArray LocationArray = new JSONArray();
		File f = new File("cpu.txt"); // TBD
		FileOutputStream fos = new FileOutputStream(f, true);
//		PrintStream ps = new PrintStream(fos);

//		int i = 0;
		
		JSONObject cpu = new JSONObject();
				
		try {
			
			statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(query);
				
			while (rs.next()) { // TBD
				
				cpu.put("type", "pc");
				
				JSONObject data = new JSONObject();
				data.put("OID", rs.getInt(KEY_ID));
				data.put("sensor", ""); // not sure how to implement
												// this
				data.put("timestamp", rs.getString(KEY_CREATED_AT));
				data.put("type", "CPU");
				data.put("platformID", DesktopSensor.platformId); // platformID
				data.put("requestID", Long.toString(DesktopSensor.requestID)); // requestID
				data.put(KEY_CPU, rs.getDouble(KEY_CPU));
				
				cpu.put("data", data);
/*
				JSONObject data = new JSONObject();
				data.put(KEY_CPU, rs.getDouble(KEY_CPU));
				// data.put(KEY_MEMORY, rs.getDouble(KEY_MEMORY));
				cpu.put("data", data);
*/

//				LocationArray.put(location);
//				i++;
				// return location;
			}

		} catch (JSONException e) {
			e.printStackTrace();
			// return false;
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// connection close failed.
				System.err.println(e);
			}
		}
//		Root.put("pc_observation", cpu);
//		ps.append(Root.toString());
		
//		System.out.println("exportToJSON_CPU=" + Root.toString());
		return cpu;
	}

	public JSONObject exportToJSON_MEMORY() throws JSONException,
			FileNotFoundException, SQLException, ClassNotFoundException {

		Class.forName("org.sqlite.JDBC");

		String dbN = "jdbc:sqlite:" + dbName;		
		connection = DriverManager.getConnection(dbN);
				
		String query = "select * from " + tableName + ";";
		
//		System.out.println("select result=" + rs.getRow());
		
		JSONObject Root = new JSONObject();
//		JSONArray LocationArray = new JSONArray();
		File f = new File("Memory.txt"); // TBD
		FileOutputStream fos = new FileOutputStream(f, true);
		PrintStream ps = new PrintStream(fos);

		int i = 0;
		JSONObject memory = new JSONObject();
					
		try {
			
			statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(query);
			
			while (rs.next()) { // TBD		
				
				memory.put("type", "pc");
				
				JSONObject data = new JSONObject();
				data.put("OID", rs.getInt(KEY_ID));
				data.put("sensor", ""); // TBD
				data.put("timestamp", rs.getString(KEY_CREATED_AT));
				data.put("type", "MEMORY"); 
				data.put("platformID", DesktopSensor.platformId); // platformID
				data.put("requestID", Long.toString(DesktopSensor.requestID)); // requestID
				
				data.put(KEY_MEMORY, rs.getDouble(KEY_MEMORY));
				memory.put("data", data);
/*
				JSONObject data = new JSONObject();
				data.put(KEY_MEMORY, rs.getDouble(KEY_MEMORY));
				location.put("data", data);
*/
//				rs.next();
//				LocationArray.put(location);
				i++;
			}
		} catch (JSONException e) {
				e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// connection close failed.
				System.err.println(e);
			}
		}
		
//		Root.put("pc_observation", memory);
//		ps.append(Root.toString());
		
//		System.out.println("exportToJSON_MEMORY=" + Root.toString());
		return memory;
	}
	
	public JSONObject exportToJSON_Process() throws JSONException,
	FileNotFoundException, SQLException, ClassNotFoundException {
		
		Class.forName("org.sqlite.JDBC");
		
		String dbN = "jdbc:sqlite:" + dbName;
		connection = DriverManager.getConnection(dbN);
				
		String query = "select * from " + processTableName + ";";
		
		JSONObject Root = new JSONObject();
		JSONArray ProcessArray = new JSONArray();
		
		JSONObject process = new JSONObject();
					
		try {
			
			statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(query);

			while (rs.next()) { // TBD
				
				process.put("type", "pc");
				
				JSONObject data = new JSONObject();
				
				data.put("OID", rs.getInt(KEY_ID));
				data.put("sensor", ""); // TBD
				data.put("timestamp", rs.getString(KEY_CREATED_AT));
				data.put("type", "PROCESS"); 
				data.put("platformID", DesktopSensor.platformId); // platformID
				data.put("requestID", Long.toString(DesktopSensor.requestID)); // requestID

//				JSONObject data = new JSONObject();
				data.put(KEY_PROCESS_IMAGE_NAME, rs.getString(KEY_PROCESS_IMAGE_NAME));
				data.put(KEY_PROCESS_SESSION_NAME, rs.getString(KEY_PROCESS_SESSION_NAME));
				data.put(KEY_PROCESS_MEMORY_USAGE, rs.getString(KEY_PROCESS_MEMORY_USAGE));
				/*
				if( !System.getProperty("os.name").contains("Windows")) 
					data.put(KEY_PROCESS_UP_TIME, rs.getString(KEY_PROCESS_UP_TIME));	
				*/			
				process.put("data", data);				
				
//				ProcessArray.put(process);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// connection close failed.
				System.err.println(e);
			}
		}
			
//		Root.put("pc_observation", process);		
//		System.out.println("exportToJSON_Process=" + process.toString());
		return process;
	}

	public static void main(String[] args) throws ClassNotFoundException {
		// load the sqlite-JDBC driver using the current class loader
		Class.forName("org.sqlite.JDBC");

		Connection connection = null;
		try {
			// create a database connection
			connection = DriverManager.getConnection("jdbc:sqlite:sample.db");
			Statement statement = connection.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.

			statement.executeUpdate("drop table if exists person");
			statement
					.executeUpdate("create table person (id integer, name string)");
			statement.executeUpdate("insert into person values(1, 'leo')");
			statement.executeUpdate("insert into person values(2, 'yui')");
			ResultSet rs = statement.executeQuery("select * from person");
			while (rs.next()) {
				// read the result set
				System.out.println("name = " + rs.getString("name"));
				System.out.println("id = " + rs.getInt("id"));
			}
		} catch (SQLException e) {
			// if the error message is "out of memory",
			// it probably means no database file is found
			System.err.println(e.getMessage());
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// connection close failed.
				System.err.println(e);
			}
		}
	}
}
