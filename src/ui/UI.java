package ui;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JToggleButton;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextArea;

import main.MacIdleTime;
import main.Win32IdleTime;

import org.json.JSONException;
import org.json.JSONObject;

import common.ServiceType;
import database.Database;
import sensor.Point;
import sensor.ProcessSensor;
import utils.HTTPHandler;

public class UI {
	private JTextArea ta;
	private JButton Registration;
	private JButton Send, SendIDLE, SendCPU, SendMemory, SendProcess, Display;
	private JButton Instatiation;
	private JToggleButton ToggleButton;
	boolean toggleOn;
	static Database db;
	private static MacIdleTime macidle;
	private static Win32IdleTime win32idle;
	List<String> processes = new ArrayList<String>();

	public UI(MacIdleTime mac, Database database) {
		UI.macidle = mac;
		UI.db = database;
		initComponents(db);

	}

	public UI(Win32IdleTime winidle, Database db2) {
		UI.win32idle = winidle;
		UI.db = db2;
		initComponents(db);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				if( macidle != null )
					new UI(macidle, db);
				else 
					new UI(win32idle, db);
			}
		});
	}

	// get status of Toggle Button
	public boolean getToggle() {
		return this.toggleOn;
	}

	// set Toggle Status
	public void setToggle(boolean status) {
		this.toggleOn = status;
	}

	// update TextArea to reflect idle status
	public void updateText(String status, String time, String cpu, String memory, String process) {
		ta.setText("Current Status: " + status + "\n"
				+ "You have been idled for " + time + "s.\n" + "CPU usage: "
				+ cpu + "%\n" + "Memory usage: " + memory + "%\n"
				+ "Number of Processes running: " + process + "\n");
	}

	// update GraphArea from data sent 
	public void updateGraph(String x_axis, String y_axis, List<Double> values) {
		ta.setText("Current X axis: " + x_axis + "\n"
				+ "Current Y axis " + y_axis + " \n" + "Number of values " + values.size() + "%\n");
	}
	
	// return Send Button
	public JButton getSendButton() {
		return this.Send;
	}

	// return Send Button
	public JButton getSendIDLEButton() {
		return this.SendIDLE;
	}

	// return Send Button
	public JButton getSendCPUButton() {
		return this.SendCPU;
	}

	// return Send Button
	public JButton getSendMemoryButton() {
		return this.SendMemory;
	}
	
	// return Send Button
	public JButton getSendProcessButton() {
		return this.SendProcess;
	}

	// return Toggle Button
	public JToggleButton getToggleButton() {
		return this.ToggleButton;
	}

	// init
	private void initComponents(Database database) {
		UI.db = database;
		if( UI.db == null )
			System.out.println("UI.db is null");
		
		JFrame f = new JFrame();
		f.setTitle("Cannonball");

		ToggleButton = new JToggleButton("Turn on tracking");
		ToggleButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (!toggleOn) {
					toggleOn = true; // set toggleOn to true
					ToggleButton.setText("Turn off tracking");
				} else {
					toggleOn = false;
					ToggleButton.setText("Turn on tracking ");
				}

			}
		});

		// Registration button
		Registration = new JButton("Registration");
		Registration.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/*
				 * JFrame ff = new JFrame();
				 * ff.setTitle("RegistrationActivity"); GroupLayout groupLayout
				 * = new GroupLayout(ff.getContentPane());
				 * ff.getContentPane().setLayout(groupLayout);
				 * 
				 * ff.pack(); ff.setVisible(true);
				 */
				Registration regis = new Registration();

			}
		});
		Registration.setToolTipText("register your device");

		// Instantiation button
		Instatiation = new JButton("Instatiation");
		Instatiation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Point entry = new Point();
				ArrayList<ProcessSensor> pList;
				
				try {
					if( macidle != null) {
						entry.setState(macidle.getStatus());
						entry.setIdleTime((int) macidle.getIdleTimeMillis() / 1000);
						entry.setCPU(macidle.getCPU());
						entry.setMemory(macidle.getMemory());
						pList = new ArrayList<ProcessSensor>(macidle.getProcessList());

						entry.setTimeStamp(entry.getDateTime());
					}
					else
					{
						entry.setState(win32idle.getStatus());
						entry.setIdleTime((int) win32idle.getIdleTimeMillisWin32() / 1000);
						entry.setCPU(win32idle.getCPU());
						entry.setMemory(win32idle.getMemory());
						pList = new ArrayList<ProcessSensor>(win32idle.getProcessList());

						entry.setTimeStamp(entry.getDateTime());
					}
					db.addEntry(entry);
					
//					System.out.println("pList size=" + pList.size());
					
					for(int i=0; i < pList.size(); i++ ) 
					{
//						System.out.println("name=" + pList.get(i).getProcessName());
						db.addProcessEntry(pList.get(i));
					}
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});

		Instatiation.setToolTipText("Save current data");

		// Send button
		Send = new JButton("Send to server");
		Send.addActionListener(new ActionListener() {
			private String objJson;

			public void actionPerformed(ActionEvent e) {
				SendIDLE.doClick();
				SendCPU.doClick();
				SendMemory.doClick();
				SendProcess.doClick();
			}
		});
		Send.setToolTipText("send data to server");
		
		// Display button
		Display = new JButton("Display");
		Display.addActionListener(new ActionListener() {
			
			// Receive JSON from TIPPERS
			public void actionPerformed(ActionEvent e) {	
				
				 SwingUtilities.invokeLater(new Runnable() {
			         public void run() {
			        	List<Double> scoresList = new ArrayList<Double>();
			            GraphPanel.createAndShowGui(scoresList);
			         }
			      });
			}
		});
		Display.setToolTipText("display data");

		// Send IDLE button
		SendIDLE = new JButton();
		SendIDLE.addActionListener(new ActionListener() {
			private String objJson;

			public void actionPerformed(ActionEvent e) {

				JSONObject success = null;
				try {
					if( db == null )
						System.out.println("db is null");
					success = db.exportToJSON_IDLE(); // CPU only
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				objJson = success.toString();
				HTTPHandler.execute(ServiceType.PUT_OBSERVATION, objJson);
				/* EUNJEONG.SHIN
				try {
					db.dropTable();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				*/


			}
		});

		// Send CPU usage button
		SendCPU = new JButton();
		SendCPU.addActionListener(new ActionListener() {
			private String objJson;

			public void actionPerformed(ActionEvent e) {

				JSONObject success = null;
				try {
					success = db.exportToJSON_CPU(); // CPU only
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				objJson = success.toString();
				HTTPHandler.execute(ServiceType.PUT_OBSERVATION, objJson);
				/* EUNJEONG.SHIN
				try {
					db.dropTable();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				*/
			}
		});

		// Send Memory Usage button
		SendMemory = new JButton();
		SendMemory.addActionListener(new ActionListener() {
			private String objJson;

			public void actionPerformed(ActionEvent e) {

				JSONObject success = null;
				try {
					success = db.exportToJSON_MEMORY(); // CPU only
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				objJson = success.toString();
				HTTPHandler.execute(ServiceType.PUT_OBSERVATION, objJson);
				/* EUNJEONG.SHIN 
				try {
					db.dropTable();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				*/
			}
		});
		
		// Send Process Usage button
		SendProcess = new JButton();
		SendProcess.addActionListener(new ActionListener() {
			private String objJson;

			public void actionPerformed(ActionEvent e) {

				JSONObject success = null;
				try {
					success = db.exportToJSON_Process(); // Process only
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				objJson = success.toString();
				HTTPHandler.execute(ServiceType.PUT_OBSERVATION, objJson);
				/* EUNJEONG.SHIN 
				try {
					db.dropTable();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				*/
			}
		});

		ta = new JTextArea();
		ta.setColumns(10);
		ta.setRows(1);
		ta.setText("Current Status: Idle" + "\n"
				+ "You have been idled for 30s.");
		ta.setText("You have not turned on tracking.");

		GroupLayout groupLayout = new GroupLayout(f.getContentPane());
		groupLayout
				.setHorizontalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addGap(90)
																		.addGroup(
																				groupLayout
																						.createParallelGroup(
																								Alignment.LEADING,
																								false)
																						.addComponent(
																								Send,
																								GroupLayout.DEFAULT_SIZE,
																								GroupLayout.DEFAULT_SIZE,
																								Short.MAX_VALUE)
																						.addComponent(
																								Instatiation,
																								GroupLayout.DEFAULT_SIZE,
																								GroupLayout.DEFAULT_SIZE,
																								Short.MAX_VALUE)
																						.addComponent(
																								Registration,
																								GroupLayout.DEFAULT_SIZE,
																								GroupLayout.DEFAULT_SIZE,
																								Short.MAX_VALUE)
																						.addComponent(
																								Display,
																								GroupLayout.DEFAULT_SIZE,
																								GroupLayout.DEFAULT_SIZE,
																								Short.MAX_VALUE)))
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addGap(39)
																		.addComponent(
																				ta,
																				GroupLayout.PREFERRED_SIZE,
																				239,
																				GroupLayout.PREFERRED_SIZE))
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addGap(92)
																		.addComponent(
																				ToggleButton)))
										.addContainerGap(62, Short.MAX_VALUE)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(
				Alignment.TRAILING).addGroup(
				groupLayout
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(ToggleButton)
						.addGap(18)
						.addComponent(ta, GroupLayout.PREFERRED_SIZE, 105,
								GroupLayout.PREFERRED_SIZE).addGap(18)
						.addComponent(Registration)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(Instatiation)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(Send).addGap(18)
						.addComponent(Display).addGap(32)));
		f.getContentPane().setLayout(groupLayout);
		f.pack();
		f.setVisible(true);
		
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
}
