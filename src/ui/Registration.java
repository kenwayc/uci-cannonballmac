package ui;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JToggleButton;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import java.sql.SQLException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextArea;

import main.MacIdleTime;
import main.Win32IdleTime;

import org.json.JSONException;
import org.json.JSONObject;

import common.ServiceType;
import database.Database;
import sensor.DesktopSensor;
import sensor.Point;
import sensor.Sensor;
import utils.HTTPHandler;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;

public class Registration {
	private JButton Registration;
	boolean toggleOn;
	static Database db;
	private static MacIdleTime macidle;


	public Registration() {
		initComponents();
	}
	/**
	 * @wbp.parser.entryPoint
	 */
	public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Registration();
            }
        });
    }
    /**
     * @wbp.parser.entryPoint
     */
    private void initComponents() {
    	
        JFrame f = new JFrame();
        f.setTitle("RegistrationActivity");

        JFormattedTextField RegistrationID = new JFormattedTextField();
        RegistrationID.setText("Enter Registration ID");
        
        // Registration button
        Registration = new JButton("Complete Registration");
        Registration.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		String user_id = RegistrationID.getText();
        		DesktopSensor.platformId = user_id; // register the device with the id provided from webs
        		f.dispose(); // close the registration window
        		
        		
        		//System.out.println(user_id);

        	}
        });
        Registration.setToolTipText("register your device");
        
        
        
        JLabel CopyDevice = new JLabel("Copy Device ID from Webs");
        GroupLayout groupLayout = new GroupLayout(f.getContentPane());
        groupLayout.setHorizontalGroup(
        	groupLayout.createParallelGroup(Alignment.LEADING)
        		.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
        			.addContainerGap(22, Short.MAX_VALUE)
        			.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
        				.addComponent(Registration, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
        				.addComponent(RegistrationID, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
        				.addComponent(CopyDevice))
        			.addGap(23))
        );
        groupLayout.setVerticalGroup(
        	groupLayout.createParallelGroup(Alignment.LEADING)
        		.addGroup(groupLayout.createSequentialGroup()
        			.addGap(20)
        			.addComponent(CopyDevice)
        			.addGap(18)
        			.addComponent(RegistrationID, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        			.addGap(18)
        			.addComponent(Registration)
        			.addGap(21))
        );
        f.getContentPane().setLayout(groupLayout);
        f.pack();
        f.setVisible(true);
        
 
    }
}
