package main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import sensor.Point;
import sensor.ProcessSensor;
import sensor.Point.State;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.management.OperatingSystemMXBean;


/**
 * @author Kangwei
 * @author Sunitha
 * Utility method to retrieve the idle time on Mac OS X 10.4+
 * @author kwindszus
 */
public class MacIdleTime {
	private String state = State.unkown;
	private String newState;
	private DateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
	private Point entry = new Point();
	private double TotalPhysicalMemory;
	
	// constructor
	public MacIdleTime() {
		TotalPhysicalMemory = this.getTotalPhysicalMemory();
	}

	// application services via jna.jar
	public interface ApplicationServices extends Library {

		ApplicationServices INSTANCE = (ApplicationServices) Native
				.loadLibrary("ApplicationServices", ApplicationServices.class);

		int kCGAnyInputEventType = ~0;
		int kCGEventSourceStatePrivate = -1;
		int kCGEventSourceStateCombinedSessionState = 0;
		int kCGEventSourceStateHIDSystemState = 1;

		/**
		 * @see http
		 *      ://developer.apple.com/mac/library/documentation/Carbon/Reference
		 *      /QuartzEventServicesRef/Reference/reference.html#//apple_ref/c/
		 *      func/CGEventSourceSecondsSinceLastEventType
		 * @param sourceStateId
		 * @param eventType
		 * @return the elapsed seconds since the last input event
		 */
		public double CGEventSourceSecondsSinceLastEventType(int sourceStateId,
				int eventType);
	}

	// get IdleTime in Milliseconds
	public static long getIdleTimeMillis() {
		double idleTimeSeconds = ApplicationServices.INSTANCE
				.CGEventSourceSecondsSinceLastEventType(
						ApplicationServices.kCGEventSourceStateCombinedSessionState,
						ApplicationServices.kCGAnyInputEventType);
		return (long) (idleTimeSeconds * 1000);
	}

	// get system CPU usage
	@SuppressWarnings("restriction")
	public double getCPU() {
		/** 
		 * be sure to import com.sun.management.OperatingSystemMXBean
		 * and change Eclipse preference settings (Java->Compiler->Error and Warnings->
		 * Deprecated and Restricted API->Forbidden reference->"Warning")
		 */
		OperatingSystemMXBean osBean = ManagementFactory
				.getPlatformMXBean(OperatingSystemMXBean.class);
		// What % load the overall system is at, from 0.0-1.0
		double cpu = osBean.getSystemCpuLoad() * 100.0;
		
		return cpu;
	}
	
	// get total physical memory size
	@SuppressWarnings("restriction")
	public double getTotalPhysicalMemory() {
		OperatingSystemMXBean osBean = ManagementFactory
				.getPlatformMXBean(OperatingSystemMXBean.class);
		double memory = osBean.getTotalPhysicalMemorySize();
		return memory;
	}
	
	// get free physical memory size
	@SuppressWarnings("restriction")
	public double getFreePhysicalMemory() {
		OperatingSystemMXBean osBean = ManagementFactory
				.getPlatformMXBean(OperatingSystemMXBean.class);
		double memory = osBean.getFreePhysicalMemorySize();
		return memory;
	}
	
	// get memory usage
	@SuppressWarnings("restriction")
	public double getMemory() {
		OperatingSystemMXBean osBean = ManagementFactory
				.getPlatformMXBean(OperatingSystemMXBean.class);
		double memory = osBean.getFreePhysicalMemorySize()/this.TotalPhysicalMemory*100.0;
		return memory;
	}
		
	// add Idle Status
	public String getStatus() throws ClassNotFoundException {
			int idleSec = (int) (getIdleTimeMillis() / 1000);

			// determine idle status: online (less than 15s), idle, away (more than 5m)
			newState = idleSec < 15 ? State.online
					: idleSec > 5 * 60 ? State.away : State.idle;

			if (newState != state) {
				state = newState;
			}
			return state;
	}
	
	public ArrayList<ProcessSensor> getProcessList() {
		String line, name = null, session, memory, time;
		
		ArrayList<ProcessSensor> processList = new ArrayList<ProcessSensor>();
		
		try {						  
//		    Process p = Runtime.getRuntime().exec("ps -e");
//			Process p = Runtime.getRuntime().exec("top");
			Process p = Runtime.getRuntime().exec("ps -ef -o command,vsize,rss,%mem");
		    BufferedReader input =
		            new BufferedReader(new InputStreamReader(p.getInputStream()));
		    
		    int lineNum = 0;
		    int timeIndex = 0, commandIndex = 0, memoryIndex = 0;
		    while ((line = input.readLine()) != null) {
//		        System.out.println(line); //<-- Parse data here.
		    	
		    	lineNum++;
		    	
//		    	System.out.println("lineNum="+ lineNum); //<-- Parse data here.
		    	
		        if ( !line.trim().equals("") ) {	
		        	
		        	if( lineNum == 1 ) {		        	
		        		timeIndex = line.lastIndexOf("TIME");
//		        		System.out.println("timeIndex=" + timeIndex);
		        		
		        		commandIndex = line.indexOf("COMMAND");
//		        		System.out.println(commandIndex);
		        				              
		        		memoryIndex = line.lastIndexOf("%MEM");
//		        		System.out.println("memoryIndex=" + memoryIndex);
		        	           
		          }	else {		           
			            name = line.substring(commandIndex, commandIndex + 50);			// keep only the process name
//			            System.out.println("name=" + name);
			          
			            if( name.toUpperCase().contains("Applications".toUpperCase()) ) {			    
			        	  	time = line.substring(timeIndex-4, timeIndex + 4);			// keep only the session name
//				            System.out.println("time=" + time);
				            
				            memory = line.substring(memoryIndex, memoryIndex + 4);		// keep only the memory usage
//			        		System.out.println("memory=" + memory);
			        		
//			        		ProcessSensor process = new ProcessSensor( name.trim(), session.trim(), memory.trim() );
			        		ProcessSensor process = new ProcessSensor( name.trim(), "", memory.trim() );
			              
			        		processList.add(process);		  
			        		
			            } else {
//			            	System.out.println("It's not applications");
			            }
		        	
		        	
		          }
		        }
		   
		    }
		    
		    input.close();
		} catch (Exception err) {
		    err.printStackTrace();
		}
		
		return processList;
		
	}
	
	// print Idle Status
	public void printIdleStatus() {
		for (;;) {
			int idleSec = (int) (getIdleTimeMillis() / 1000);

			// determine idle status: online (less than 15s), idle, away (more than 5m)
			newState = idleSec < 15 ? State.online
					: idleSec > 5 * 60 ? State.away : State.idle;

			if (newState.equals(state)) {
				state = newState;
				System.out.println(dateFormat.format(new Date()) + " # "
						+ state);
			}
			try {
				Thread.sleep(1000);
			} catch (Exception ex) {
			}
		}
	}

	public static void main(String[] args) {
		if (!System.getProperty("os.name").contains("Mac")) {
			System.err.println("ERROR: Only implemented on Mac");
			System.exit(1);
		}
		//State state = State.UNKNOWN;
		//DateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");

		MacIdleTime macidle = new MacIdleTime();
		for (;;) {
			System.out.println(macidle.getCPU() + "%  " + macidle.getMemory());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
/*			int idleSec = (int) (getIdleTimeMillis() / 1000);

			//State newState = idleSec < 30 ? State.ONLINE
					//: idleSec > 5 * 60 ? State.AWAY : State.IDLE;

			if (newState != state) {
				state = newState;
				System.out.println(dateFormat.format(new Date()) + " # "
						+ state);
			}
			try {
				Thread.sleep(1000);
			} catch (Exception ex) {
			}*/
		}
	}
	
	
}
