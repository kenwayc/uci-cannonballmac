package main;

import java.net.URISyntaxException;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;

import client.Listener;
import sensor.Point;
import sensor.ProcessSensor;
import sensor.Point.State;
import ui.UI;
import database.Database;

/**
 * 
 * @author kenwaychan
 *
 */
public class MainActivity {
	static Point entry;
	static String previous_state;
	static String current_state;
	static Database db;
	static DecimalFormat df = new DecimalFormat("#.##");
	static Double cpu, memory;
	static Integer process;
	private static Listener listener;

	public static void main(String[] args) throws ClassNotFoundException, URISyntaxException {
		if (!(System.getProperty("os.name").contains("Mac") || System
				.getProperty("os.name").contains("Win"))) {
			System.err.println("ERROR: Only implemented on Mac and Windows");
			System.exit(1);
		}
		
		ArrayList<ProcessSensor> pList;
		
		if (System.getProperty("os.name").contains("Mac")) {
			MacIdleTime macidle = new MacIdleTime();
			db = new Database("pc.db");
			db.createDB();
			UI ui = new UI(macidle, db);		
			listener = new Listener(macidle, db, ui);
			
			previous_state = State.unkown;
			entry = new Point();
			
			int idle_time;
			for (;;) {
				current_state = macidle.getStatus();
				idle_time = (int) macidle.getIdleTimeMillis() / 1000;
				if (ui.getToggle()) {
					cpu = macidle.getCPU();
					memory = macidle.getMemory();
					process = macidle.getProcessList().size();
					ui.updateText(current_state, idle_time + "", df.format(cpu) + "", df.format(memory) + "", df.format(process) + "" );

					entry.setState(current_state);
					entry.setIdleTime(idle_time);
					entry.setCPU(cpu);
					entry.setMemory(memory);
					entry.setTimeStamp(entry.getDateTime());
					db.addEntry(entry);
					
					pList = new ArrayList<ProcessSensor>(macidle.getProcessList());
					for(int i=0; i < pList.size(); i++ ) 
					{
//						System.out.println("name=" + pList.get(i).getProcessName());
						db.addProcessEntry(pList.get(i));
					}
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ex) {
				}
			}
		} else {
					
			Win32IdleTime winidle = new Win32IdleTime();								
			db = new Database("pc.db");
			db.createDB();
//			System.out.println("db created");
				
			UI ui = new UI(winidle, db);			
			listener = new Listener(winidle, db, ui);
			
			previous_state = State.unkown;
//			current_state = State.unkown;		
			entry = new Point();
			int idle_time;
			for (;;) {
				current_state = winidle.getStatus();
				idle_time = (int) winidle.getIdleTimeMillisWin32() / 1000;
//				System.out.println(current_state);
				// ui.updateString("Current State: " + current_state);
/*
				if (!current_state.equals(previous_state)) {
					entry.setState(current_state);
					entry.setTimeStamp(entry.getDateTime());
					db.addEntry(entry);
					System.out.println(entry.getTimeStamp() + " # "
							+ current_state);
					previous_state = current_state;
				}
*/				
				if (ui.getToggle()) {
					// ui.updateText(current_state,
					// winidle.getIdleTimeMillisWin32()/1000+"");
					
					cpu = winidle.getCPU();
					memory = winidle.getMemory();
					process = winidle.getProcessList().size();
					ui.updateText(current_state, idle_time + "", df.format(cpu)
							+ "", df.format(memory) + "" , df.format(process) + "");

					entry.setState(current_state);
					entry.setIdleTime(idle_time);
					entry.setCPU(cpu);
					entry.setMemory(memory);
					entry.setTimeStamp(entry.getDateTime());
					db.addEntry(entry);
				
					pList = new ArrayList<ProcessSensor>(winidle.getProcessList());
					for(int i=0; i < pList.size(); i++ ) 
					{
//						System.out.println("name=" + pList.get(i).getProcessName());
						db.addProcessEntry(pList.get(i));
					}
				}

				try {
					Thread.sleep(1000);
				} catch (Exception ex) {
				}
			}
		} // to be tested

		// macidle.getIdleStatus();

		/*
		 * Point p = new Point(); p.setState(State.away);
		 * p.setTimeStamp(p.getDateTime()); db.addEntry(p);
		 * p.setState(State.online); p.setTimeStamp(p.getDateTime());
		 * db.addEntry(p);
		 */

		// db.addEntry();
		// List<Point> entries = db.getAllEntries();
		// System.out.println(entries.toString());

	}
}
