package main;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.ThreadMXBean;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.management.MBeanServerConnection;

import sensor.Point;
import sensor.Point.State;
import sensor.ProcessSensor;

import com.sun.jna.*;
import com.sun.jna.win32.*;

/**
 * Utility method to retrieve the idle time on Windows and sample code to test
 * it. JNA shall be present in your classpath for this to work (and compile).
 * 
 * @author ochafik
 */
public class Win32IdleTime {
	private static String state = State.unkown;
	private String newState;
	private DateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
	private Point entry = new Point();
	
	public Win32IdleTime() {
		
	}

	public interface Kernel32 extends StdCallLibrary {
		Kernel32 INSTANCE = (Kernel32) Native.loadLibrary("kernel32",
				Kernel32.class);

		/**
		 * Retrieves the number of milliseconds that have elapsed since the
		 * system was started.
		 * 
		 * @see http://msdn2.microsoft.com/en-us/library/ms724408.aspx
		 * @return number of milliseconds that have elapsed since the system was
		 *         started.
		 */
		public int GetTickCount();
	};

	public interface User32 extends StdCallLibrary {
		User32 INSTANCE = (User32) Native.loadLibrary("user32", User32.class);

		/**
		 * Contains the time of the last input.
		 * 
		 * @see http
		 *      ://msdn.microsoft.com/library/default.asp?url=/library/en-us/
		 *      winui/winui/windowsuserinterface/userinput/keyboardinput/
		 *      keyboardinputreference/keyboardinputstructures/lastinputinfo.asp
		 */
		public static class LASTINPUTINFO extends Structure {
			public int cbSize = 8;

			// / Tick count of when the last input event was received.
			public int dwTime;

			@Override
			 protected List getFieldOrder() {
	                return Arrays.asList(new String[] { "cbSize", "dwTime" });
	         }
		}

		/**
		 * Retrieves the time of the last input event.
		 * 
		 * @see http
		 *      ://msdn.microsoft.com/library/default.asp?url=/library/en-us/
		 *      winui/winui/windowsuserinterface/userinput/keyboardinput/
		 *      keyboardinputreference
		 *      /keyboardinputfunctions/getlastinputinfo.asp
		 * @return time of the last input event, in milliseconds
		 */
		public boolean GetLastInputInfo(LASTINPUTINFO result);
	};

	/**
	 * Get the amount of milliseconds that have elapsed since the last input
	 * event (mouse or keyboard)
	 * 
	 * @return idle time in milliseconds
	 */
	public static int getIdleTimeMillisWin32() {
//		System.out.println("getIdleTimeMillisWin32 entered");
		User32.LASTINPUTINFO lastInputInfo = new User32.LASTINPUTINFO();
		User32.INSTANCE.GetLastInputInfo(lastInputInfo);
		return Kernel32.INSTANCE.GetTickCount() - lastInputInfo.dwTime;
	}


	
	// add Idle Status
	public String getStatus() throws ClassNotFoundException {
		//for (;;) {
			int idleSec = (int) (getIdleTimeMillisWin32() / 1000);
			
//			System.out.println("idle second=" + idleSec);

			// determine idle status: online (less than 30s), idle, away (more than 5m)
			newState = idleSec < 30 ? State.online
					: idleSec > 5 * 60 ? State.away : State.idle;

//			System.out.println("newState=" + newState);
					
			if (newState != state) {
				state = newState;
				//entry.setState(state);
			}
/*			try {
				Thread.sleep(1000);
			} catch (Exception ex) {
			}*/
			
//			System.out.println("state=" + state);
			
			return state;
		//}
	}

	public static void main(String[] args) {
		if (!System.getProperty("os.name").contains("Windows")) {
			System.err.println("ERROR: Only implemented on Windows");
			System.exit(1);
		}
		state = State.unkown;
		DateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");

		for (;;) {
			int idleSec = getIdleTimeMillisWin32() / 1000;

			String newState = idleSec < 30 ? State.online
					: idleSec > 5 * 60 ? State.away : State.idle;

			if (newState != state) {
				state = newState;
				System.out.println(dateFormat.format(new Date()) + " # "
						+ state);
			}
			try {
				Thread.sleep(1000);
			} catch (Exception ex) {
			}
		}
	}



	public Double getCPU() {
		/*
		ThreadMXBean bean = ManagementFactory.getThreadMXBean();
		Long cpuTime = new Long(bean.getCurrentThreadCpuTime());
		
		return bean.isCurrentThreadCpuTimeSupported()? cpuTime.doubleValue(): 0D;
		*/
			
		/*	This function is not supported on Windows sometimes.
			OperatingSystemMXBean bean = ManagementFactory.getOperatingSystemMXBean();
		
			return (double) (osMBean.getSystemLoadAverage() / osMBean.getAvailableProcessors());
		*/	
		 OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
		  for (Method method : operatingSystemMXBean.getClass().getDeclaredMethods()) {
		    method.setAccessible(true);
		    if (method.getName().startsWith("get") 
		        && Modifier.isPublic(method.getModifiers())) {
		            Object value;
		        try {
		            value = method.invoke(operatingSystemMXBean);
		        } catch (Exception e) {
		            value = e;
		        } // try
//		        System.out.println(method.getName() + " = " + value);
		        if( method.getName().equals("getProcessCpuLoad") )
		        	return (Double) value;
		        	
		    } // if
		  } // for
		  
		  return 0D;
	}



	public Double getMemory() {
		long total = Runtime.getRuntime().totalMemory();
		long used = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		
		Long lTotal = new Long(total);
		Long uTotal = new Long(used);
		
		return uTotal.doubleValue()/lTotal.doubleValue();
	}
	
	public ArrayList<ProcessSensor> getProcessList() {
		String line, name, session, memory;
		
		ArrayList<ProcessSensor> processList = new ArrayList<ProcessSensor>();
		
		try {
			Process p = Runtime.getRuntime().exec
				    (System.getenv("windir") +"\\system32\\"+"tasklist.exe");
		    BufferedReader input =
		            new BufferedReader(new InputStreamReader(p.getInputStream()));
		    int lineNum = 0;
		    while ((line = input.readLine()) != null) {
//			    System.out.println(line); //<-- Parse data here.
		    	
			    lineNum++;
			    
//			    System.out.println("lineNumber=" + lineNum);
			    
			    if ( (!line.trim().equals("")) && (lineNum > 3)) {			  
			    				    		
		              name = line.substring(0, 24);			// keep only the process name
//		              System.out.println("name=" + name);
		              session = line.substring(35, 51);		// keep only the session name
//		              System.out.println("session=" + session);
		              memory = line.substring(64, 76);		// keep only the memory usage
//		              System.out.println("memory=" + memory);
		              
		              ProcessSensor process = new ProcessSensor( name.trim(), session.trim(), memory.trim() );
		              
		              processList.add(process);
		             
		          }				       
		    }
		    
//		    System.out.println("getProcessList()=" + processList.size());
		    
		    input.close();
		} catch (Exception err) {
		    err.printStackTrace();
		}
		
		return processList;
	}

}