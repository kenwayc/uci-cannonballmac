package utils;


import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import common.ServiceType;

/**
 * Created by joyceshin on 2015. 4. 30..
 */
@SuppressWarnings("deprecation")
public class HTTPHandler {

    private static String TAG = "HTTPHandler";

    // JOYCE SHIN
    @SuppressWarnings("resource")
	public static HttpResponse makeRequest(String uri, String json) {
        try {
            HttpPut httpPut = new HttpPut(uri);
            //Log.d(TAG, "URI:" + uri + "\n" + "json:" + json);
            httpPut.setEntity(new StringEntity(json));
            httpPut.setHeader("Accept", "application/json");
            httpPut.setHeader("Content-type", "application/json");
            return new DefaultHttpClient().execute(httpPut);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void execute(ServiceType sType, String json) {
        /*
        Map<String, String> comment = new HashMap<String, String>();
        comment.put("subject", "Using the GSON library");
        comment.put("message", "Using libraries is convenient.");
        String json = new GsonBuilder().create().toJson(comment, Map.class);
        */
    	HttpResponse response;

        switch( sType )
        {
            case PUT_OBSERVATION:
            {
//                makeRequest(Constants.SERVER_ADDRESS_PUT + "/cannonballPutObservations/", json);
            	response = makeRequest(Constants.SERVER_ADDRESS_PUT + "/request/resource/observations/", json);
            	if( response.getStatusLine().getStatusCode() == 200 )
            		System.out.println("Put observation success");
            	else
            		System.out.println("Put observation fail");
                break;
            }

            case PUT_PLATFORM:
            {
                makeRequest(Constants.SERVER_ADDRESS_PUT + "/cannonballGetPlatform/ID/", json);
                break;

            }
            
            case PUT_LOG:
            {
            	makeRequest(Constants.LOG_MANAGER + "/cannonballPutLogManager/", json);
//            	makeRequest("http://sensoria.ics.uci.edu:8000/LogManager", json);
                break;

            }

            default:
                break;
        }
    }
}
