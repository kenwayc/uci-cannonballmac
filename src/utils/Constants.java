package utils;

public class Constants {

    public static final String SERVER_ADDRESS = "http://sensoria.ics.uci.edu:3010";
//    public static final String SERVER_ADDRESS_PUT = "http://sensoria.ics.uci.edu:3009";
    public static final String SERVER_ADDRESS_PUT = "http://sensoria.ics.uci.edu:8001";
    public static final String LOG_MANAGER = "http://sensoria.ics.uci.edu:8000";

}

