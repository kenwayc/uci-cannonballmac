package utils;

public class IDHandler {

    public static String platformID = "";

    public void setPlatformID(String platformID) {
        this.platformID = platformID;
    }

    public String getPlatformID() {
        return this.platformID;
    }
}

